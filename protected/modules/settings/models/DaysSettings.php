<?php

/**
 * This is the model class for table "days_settings".
 *
 * The followings are the available columns in table 'days_settings':
 * @property integer $id
 * @property integer $day_id
 * @property integer $platform_category_settings_id
 * @property integer $obj_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property DaySettings $Day
 */
class DaysSettings extends SettingsBaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'days_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('day_id, platform_category_settings_id,', 'required'),
			array('day_id,obj_id, platform_category_settings_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, day_id, platform_category_settings_id, updated_at, created_at, created_by, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'Day' => array(self::BELONGS_TO, 'DaySettings', 'day_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'day_id' => 'Day',
			'platform_category_settings_id' => 'Platform Category Settings',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('day_id',$this->day_id);
		$criteria->compare('obj_id',$this->obj_id);
		$criteria->compare('platform_category_settings_id',$this->platform_category_settings_id);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaysSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
