<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'SORTECHS',
    'timeZone' => 'Asia/Dubai',
    // preloading 'log' component
    'preload'=>array('log',
        'booster'),
    //'theme'=>'adminpanel',
    // autoloading model and component classes
    'defaultController'=>'postQueue/main/',
    'import'=>array(
        'application.models.*',
        'application.modules.settings.models.*',
        'application.models.mange_post_template.*',
        'application.components.*',
        'application.extensions.shorturl.ShortUrl',
        'application.extensions.SimpleHTMLDOM.SimpleHTMLDOM',
        'application.extensions.eguiders.EGuider.*',
        'application.extensions.eguiders.*',
        'application.components.twitter_text.*',
        'application.vendors.phpwkhtmltopdf.WkHtmlToPdf',
        'application.modules.booster.helpers.TbHtml',
        'application.extensions.EAjaxUpload.*',
        'application.extensions.coco.*',
        'application.extensions.phpmailer.JPhpMailer',
        'application.modules.management.models.Logs',

    ),
    'homeUrl'=>array('/postQueue/main'),
    'modules'=>array(
        'thematic_post'=>array(
            'defaultController' => 'tPHome'
        ),

        'widget',

        'split_report',
        'settings'=>array('defaultController' => 'SHome'),
        // uncomment the following to enable the Gii tool

        'management',
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'123123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
      /*  'pdfable'=>array(
            'class'=>'ext.pdfable.pdfable.PdfableModule',
            'bin' => 'application/vendors/phpwkhtmltopdf',
        ),*/


        'srbac'=>array(
         /*    'applicationLayout'=>'application.views.layouts.main',
            'authAssignmentsManagerRole'=>' Auth Assignments Manager',
            'authenticatedRole'=>'Authenticated',
            'authItemsManagerRole'=>'Auth Items Manager',
            'baseScriptUrl'=>null,
            'baseUrl'=>null,
            'cssFile'=>null,
            'development'=>false,
            'exclude'=>'rbam',
            'guestRole'=>'Guest',
            'initialise'=>null,
            'layout'=>'rbam.views.layouts.main',
            'juiCssFile'=>'jquery-ui.css',
            'juiHide'=>'puff',
            'juiScriptFile'=>'jquery-ui.min.js',
            'juiScriptUrl'=>null,
            'juiShow'=>'fade',
            'juiTheme'=>'base',
            'juiThemeUrl'=>null,
            'pageSize'=>30,
            'rbacManagerRole'=>'RBAC Manager',
            'relationshipsPageSize'=>10,
            'showConfirmation'=>1000,
            'showMenu'=>true,
            'userClass'=>'User',
            'userCriteria=>array()',
            'userIdAttribute'=>'id',
            'userNameAttribute'=>'username',*/

            "userclass"=>"User",
            // Your users' table user_id column (default: userid)
            "userid"=>"id",
            // your users' table username column (default: username)
            "username"=>"username",
            // If in debug mode (default: false)
            // In debug mode every user (even guest) can admin srbac, also
            //if you use internationalization untranslated words/phrases
            //will be marked with a red star
            "debug"=>true,
            //The delimeter between modulename and auth item name for authitems in modules
            // (default "-")
            "delimeter"=>"@",

            // The number of items shown in each page (default:15)
            "pageSize"=>8,
            // The name of the super user
            "superUser" =>"Authority",
            //The name of the css to use
            "css"=>"",
            //The layout to use
            "layout"=>"application.views.layouts.admin",
            //The not authorized page
            "notAuthorizedView"=>"application.views.site.unauthorized",
            // The always allowed actions
            "alwaysAllowed"=>array(
                'SiteLogin','SiteLogout','SiteIndex','SiteAdmin','SiteError',
                'SiteContact'),
            // The operationa assigned to users
            "userActions"=>array(
                "Show","View","List"
            ),
            // The number of lines of the listboxes
            "listBoxNumberOfLines" => 10,
            // The path to the custom images relative to basePath (default the srbac images path)
            //"imagesPath"=>"../images",
            //The icons pack to use (noia, tango)
            "imagesPack"=>"noia",
            // Whether to show text next to the menu icons (default false)
            "iconText"=>true,
        )

    ),


    // application components
    'components'=>array(
        'bitly' => array(
            'class' => 'application.extensions.bitly.VGBitly',
            'login' => 'nusrv2016', // login name
            'apiKey' => 'R_a33f2aefe0dd487f8c7d1e906fcfd2a0', // apikey
            'format' => 'json', // default format of the response this can be either xml, json (some callbacks support txt as well)
        ),

        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),
        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
            'itemTable'=>'items',
            // The assignmentTable name (default:authassignment)
            'assignmentTable'=>'assignments',
            // The itemChildTable name (default:authitemchild)
            'itemChildTable'=>'itemchildren',
        ),
        'session' => array(
            'class' => 'CDbHttpSession',
            'connectionID' => 'db',
            'autoCreateSessionTable' => true    // for performance reasons
        ),
        'booster' => array(
            'class' => 'application.modules.booster.components.Booster',
        ),
        's3'=>array(
            'class'=>'ext.s3.ES3',
            'aKey'=>'AKIAIJQZJN4NL3DG767Q',
            'sKey'=>'x6J/+rRFBcvY9C34lnuk0aLV8eusV0baTXHR2Tjf',
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager'=>array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'urlSuffix' => '.html',
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db'=>require(dirname(__FILE__).'/database.php'),

        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                 //   'class'=>'CProfileLogRoute',
                 //   'levels'=>'profile',
                 //   'enabled'=>true,
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>(YII_DEBUG === true)?require(dirname(__FILE__).'/params_staging.php'):require(dirname(__FILE__).'/params.php'),
);
