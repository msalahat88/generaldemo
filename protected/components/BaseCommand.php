<?php
class BaseCommand extends CConsoleCommand
{


    private $details;
    private $category;
    private $model;
    private $trim_hash;
    private $hash_tag;
    private $platform;
    private $platform_id;
    private $parent = null;
    private $creator;
    private $sub_category;
    private $source;
    private $news;
    private $PostQueue;
    private $post_id;

    public function bitShort($url){
        $d = Yii::app()->bitly->shorten($url)->getResponseData();
        $d = CJSON::decode($d);
        if(isset($d['status_code']) && $d['status_code'] == 200){
            if(isset($d['data'])){
                $data = $d['data'];
                if(isset($data['url'])){
                    return $data['url'];
                }
            }
        }

        return null;
    }

    public function TimeZone()
    {
        $data = Settings::model()->find('category_id is NULL');
        Yii::app()->timeZone = 'Asia/Dubai';
        if(!empty($data))
            Yii::app()->timeZone = empty($data->timezone)?'Asia/Dubai':$data->timezone;
    }

    public function upload_pdf($file){
        $file = Yii::app()->params['webroot'].$file;
        $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
        $filename='pdf/pdf_'.time().'.'.$ext;
        if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ))
            return  'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;

        return null;
    }

    protected function Load()
    {
        $FACEBOOK_APP_ID = Yii::app()->params['facebook']['app_id']; // Your facebook app ID
        $FACEBOOK_SECRET = Yii::app()->params['facebook']['secret']; // Your facebook secret
        $ACCESS_TOKEN = Yii::app()->params['facebook']['token']; // The access token you receieved above
        $facebook = new Facebook\Facebook([
            'app_id' => $FACEBOOK_APP_ID,
            'app_secret' => $FACEBOOK_SECRET,
            'default_graph_version' => 'v2.5',
        ]);

        $PAGE_TOKEN = null;
        try {
            $res = $facebook->get('/me/accounts', $ACCESS_TOKEN);
            if(json_decode($res->getBody())->data){
                foreach (json_decode($res->getBody())->data as $item) {
                    if (Yii::app()->params['facebook']['page_id'] == $item->id) {
                        $PAGE_TOKEN = $item->access_token;
                        break;
                    }
                }
            }


        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return array($facebook, $PAGE_TOKEN);
    }


    function instagram_image($photo, $caption,$auth){
        $i = new Instagram($auth['username'], $auth['password'], $auth['debug']);
        try {
            $i->login();
        } catch (InstagramException $e) {
            echo $e->getMessage();
            return false;
        }
        try {
           return $i->uploadPhoto($photo, $caption);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

    }

    public function Obj_twitter(){
        $auth = Yii::app()->params['twitter'];
        Codebird::setConsumerKey($auth['key'], $auth['secret']);
        $cb = Codebird::getInstance();
        $cb->setToken($auth['token'], $auth['token_secret']);
        return $cb;
    }

    public function Size_video($file){
        $getID3 = new getID3;
        $file = $getID3->analyze($file);
        if($file['filesize']<15000000){
            $time = explode(':',$file['playtime_string']);
            if(isset($time[0]) && $time[0]<=0){
                if(isset($time[0]) && $time[1]<=30){
                    return true;
                }
            }
        }
        return false;
    }


    public function upload_file($file){
        Yii::import('application.components.S3');
        if(!empty($file)){
            $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
            $path = $file;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
            $filename='cron_'.time().'.'.$ext;
            $fileUrl='';
            if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ)) {
                $fileUrl = 'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;
            }
            unlink($file);
            return array(true,$fileUrl);
        }else{
            return array(false,'');
        }
    }

    protected function LoadPostTemplate(){

        return PostTemplate::model()->findAll('deleted = 0');
    }

    protected function LoadPlatForm(){

        return CHtml::listData(Platform::model()->findAll('deleted = 0 '),'id','title');
    }

    protected function GetMedia($id){
        return MediaNews::model()->findAll('news_id = '.$id);
    }



    protected function checkCategory($title){

        return Category::model()->findByAttributes(
            array(
                'title'=>$title
            )
        );
    }

    protected function checkSubCategory($title){
        return SubCategories::model()->findByAttributes(
            array(
                'title'=>$title
            )
        );
    }

    protected function make_image_square($image, $square = 0)
    {
        $mask = new \Imagick();
        $mask->readImage($image);
        $mask->setImageResolution(144,144);
        $d = $mask->getImageGeometry();
        $h = $d['height'];
        $w = $d['width'];
        $type = $h > $w?'height':'width';
        echo $q = ($d[$type]/2)/2;
        $obj = new Imagick();
        $obj->newImage($d[$type], $d[$type], new ImagickPixel('#d7d6db'));
        $obj->setImageResolution(144,144);
        $obj->setImageFormat('png');
        $obj->compositeImage($mask, Imagick::COMPOSITE_DEFAULT,0,$q);
        $obj->writeImage($image);
        /*
        $base = new \Imagick();
        $base->readImage($image);
        $base->setImageResolution(144,144);
        $base->resampleImage  (288,288,imagick::FILTER_UNDEFINED,1);
        if ($square)
        {
            $d = $base->getImageGeometry();
            $h = $d['height'];
            $w = $d['width'];
            $base_image = $h > $w?$w:$h;
            $base->cropImage($base_image, $base_image,0,0);
            //$base->resizeImage(700, 700, Imagick::FILTER_LANCZOS, 1);
        }
        $base->setImageResolution(144,144);
        //$base->setImageResolution(1000,1500);


        $base->writeImage($image);*/
    }


    protected function Month($index){

        $months = array(
            "01" => "يناير",
            "02" => "فبراير",
            "03" => "مارس",
            "04" => "أبريل",
            "05" => "مايو",
            "06" => "يونيو",
            "07" => "يوليو",
            "08" => "أغسطس",
            "09" => "سبتمبر",
            "10" => "أكتوبر",
            "11" => "نوفمبر",
            "12" => "ديسمبر"
        );

        return array_search($index,$months);
    }

    protected function LoadTemplate($id_category){

    }

    protected function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function getTweetLength($tweet, $gotImage ,$video) {
        $tcoLengthHttp = 23;
        $tcoLengthHttps = 23;
        $twitterPicLength = 24;
        $twitterVideoLength = 24;
        $urlData = Twitter_Extractor::create($tweet)->extract();
        $tweetLength = mb_strlen($tweet,'utf-8');
        foreach ($urlData['urls_with_indices'] as $url) {
            $tweetLength -= mb_strlen($url['url']);
            $tweetLength += mb_stristr($url['url'], 'https') === 0 ? $tcoLengthHttps : $tcoLengthHttp;
        }
        if ($gotImage) $tweetLength += $twitterPicLength;
        if ($video) $tweetLength += $twitterVideoLength;
        return $tweetLength;
    }

    public function shorten($input, $length, $ellipses = true, $strip_html = true) {
        if ($strip_html) {
            $input = strip_tags($input);
        }
        if (mb_strlen($input,'UTF-8') <= $length) {
            return $input;
        }
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = mb_substr($input, 0, $last_space);
        if ($ellipses) {
            $trimmed_text .= '...';
        }
        return $trimmed_text;
    }

    public function shorten_point($input, $point = '.') {
        $input_ex = explode($point,$input);
        $data = array_chunk($input_ex,2);
        if(is_array($data)){

            return (implode('. '.PHP_EOL,$data[0])).'. ';
        }
        return $input;
    }
    /*

   public function shorten_point($input, $point = '.') {
       $input_ex = explode($point,$input);
       $data = array_chunk($input_ex,5);
       if(is_array($data)){
           $data =  implode('. '.PHP_EOL,$data[0]);
           if(mb_strlen($data,'utf-8') > 2000){
               $data = array_chunk($input_ex,3);
               if(is_array($data)){
                   $data =  implode('. '.PHP_EOL,$data[0]);
                   if(mb_strlen($data,'utf-8') > 2000){
                       $data = array_chunk($input_ex,1);
                       return  $data =  implode('. '.PHP_EOL,$data[0]);
                   }
               }
               return $data;
           }
           return $data;
       }
       return $input;
   }
   */
    public function send_email($title,$message){

        $mail = new JPhpMailer;

        $mail->SMTPAuth = true;
        /* $mail->Username = 'Admin';
         $mail->Password = '123';*/
        $mail->SetFrom('system@sortechs.com', 'System');
        $mail->Subject = 'Error - '.$title;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $msg_contents_header = '<h2>'.$title.'</h2>';
        $msg_contents_body = '<h3>'.$message.'<br /><br />Sincerely<br />Sortechs team';
        $mail->MsgHTML($msg_contents_header.$msg_contents_body);
        /*        $mail->AddAttachment('http://www.anazahra.com/wp-content/gallery/lamia-abdeen-abayas14062016/ubuntu-tribe-collection-14.jpg','Pic');*/
        foreach (Yii::app()->params['email'] as $param) {
            $mail->AddAddress($param, 'Mohammad');
        }
        $mail->Send();
    }
    public function send_Pdf_email($user_downloaded_pdf,$name,$l_name,$email,$subject,$MSGHTML,$link,$from_date,$to_date){

        $mail = new JPhpMailer;

        $mail->SMTPAuth = true;
        /* $mail->Username = 'Admin';
         $mail->Password = '123';*/
        $mail->SetFrom('system@sortechs.com', $user_downloaded_pdf);
        $mail->Subject = $subject;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $msg_contents_header = '<h2>'.$name." ".$l_name." Content calender file".'[ '.$from_date. ' _ '.$to_date .']</h2>';
        $msg_contents_body = '<h3>Hello '.$name." ".$l_name.'</h3>Your content calender for the period ['.$from_date. ' _ '.$to_date .'] is now ready. Click on the button below to be taken to it:<br /><br />'.'<a href="'.$link.'" style="border:1px solid black;padding:8px;color:white;background-color:#8064A2;text-decoration:none;margin-top:10px;margin-bottom:10px;border-radius:8px;">'.$MSGHTML.'</a><br /><br />Sincerely<br />Sortechs team';
        $mail->MsgHTML($msg_contents_header.$msg_contents_body);
        /*        $mail->AddAttachment('http://www.anazahra.com/wp-content/gallery/lamia-abdeen-abayas14062016/ubuntu-tribe-collection-14.jpg','Pic');*/
        $mail->AddAddress($email, $name);
        $mail->Send();

    }


    public function crop_image($number , $type ,$mask){
        //split the cropping for to sides
        $number = ceil($number/2);

        //get the image dimensions
        $size=$mask->getImageGeometry();
        $w = $size['width'];
        $h = $size['height'];
        //dimensions ends here

        if($type == 'h'){
            $mask->cropImage(0, $size['height']-$number,0,$number);
            $mask->cropImage(0, $size['height']-$number,0,0);

        }else{
            $mask->cropImage($size['width']-$number, 0,$number,0);
            $mask->cropImage($size['width']-$number, 0,0,0);
        }
        return $mask;
    }

    public function scaleImage($imagePath) {
        $imagick = new \Imagick();
        $imagick->readImage($imagePath);
        $imagick->setImageResolution(144,144);

        //get the image dimensions
        $size=$imagick->getImageGeometry();

        $w = $size['width'];
        $h = $size['height'];
        $big=$w>$h?$w:$h;
        $big_type=$w>$h?'w':'h';
        //dimensions ends here

        //crop image with the extra size if exist
        $extra_size_counter=0;

        //images must have an aspect ratio between the from and to
        $from=ceil((1/1.91)*$big);
        $to=floor((4/5)*$big);
        //extra size for image if not compatible with instagram
        $extra_size=10;

        if($big_type=='w'){
            if($h<$from){
                //make the image compatible with instagram
                while (true){
                    $extra_size_counter+=$extra_size;
                    $big+=$extra_size;
                    //$imagick->scaleImage($big, 0);
                    $imagick->resizeImage($big, 0,imagick::FILTER_LANCZOS, 1);
                    $imagick->setImageResolution(144,144);
                    $size=$imagick->getImageGeometry();
                    if($size['height']>=$from){
                        break;
                    }
                }
            }
        }else{
            if($w<$to){
                //make the image compatible with instagram
                //$imagick->scaleImage($to, 0);
                $imagick->resizeImage($to, 0,imagick::FILTER_LANCZOS, 1);
                $size=$imagick->getImageGeometry();
                $extra_size_counter=$size['height']-$big;

            }
        }

        //fix the resolution


        if($extra_size_counter){
            //crop image if needed
            $imagick=$this->crop_image($extra_size_counter , $big_type ,$imagick);
        }
        //fix the resolution




        $imagick->writeImage($imagePath);
        return $imagick;
    }



    public function scaleImage_new_resolution_problem($imagePath) {
        $imagick = new \Imagick();
        $imagick->readImage($imagePath);
        $imagick->setImageCompressionQuality(100);
        $imagick->setImageResolution(144,144);

        //get the image dimensions
        $size=$imagick->getImageGeometry();

        $w = $size['width'];
        $h = $size['height'];
        $big=$w>$h?$w:$h;
        $big_type=$w>$h?'w':'h';
        //dimensions ends here

        //crop image with the extra size if exist
        $extra_size_counter=0;

        //images must have an aspect ratio between the from and to
        $from=ceil((1/1.91)*$big);
        $to=floor((4/5)*$big);
        //extra size for image if not compatible with instagram
        $extra_size=10;

        if($big_type=='w'){
            if($h<$from){


                if($big>1080){
                    $imagick=$this->resize_image(1080,0,$imagick);

                }

                $resize=ceil(($big-$h)/4);
                //echo $resize;die;
                $imagick=$this->resize_image($big-$resize,0,$imagick);


                $size=$imagick->getImageGeometry();

                $w = $size['width'];
                $h = $size['height'];
                $big=$w>$h?$w:$h;
                $from=ceil((1/1.91)*$big);

                //make the image compatible with instagram
                $imagick=$this->resize_image(0,$from,$imagick);

                $size=$imagick->getImageGeometry();
                $extra_size_counter=$size['width']-$big;

            }
        }else{
            if($w<$to){
                //
                if($big>1350){
                    $imagick=$this->resize_image(0,1350,$imagick);
                }

                $resize=ceil(($big-$h)/4);
                $imagick=$this->resize_image(0,$big-$resize,$imagick);
                $size=$imagick->getImageGeometry();

                $w = $size['width'];
                $h = $size['height'];
                $big=$w>$h?$w:$h;
                $to=ceil((1/1.91)*$big);

                //make the image compatible with instagram
                $imagick=$this->resize_image($to,0,$imagick);
                $size=$imagick->getImageGeometry();
                $extra_size_counter=$size['height']-$big;

            }
        }

        //fix the resolution


        if($extra_size_counter){
            //crop image if needed
            $imagick=$this->crop_image($extra_size_counter , $big_type ,$imagick);
        }
        //fix the resolution




        $imagick->writeImage($imagePath);

        return $imagick;
    }



    public function resize_image($w,$h,$obj){
        //$obj->scaleImage($w, $h);
        $obj->resizeImage($w, $h,imagick::FILTER_LANCZOS, 1);

        return $obj;
    }

    protected function clear_tags($string){

        $string = strip_tags($string);
        $string = str_replace('&nbsp;','',$string);
        $string = str_replace('&raquo;','',$string);
        $string = str_replace('&laquo;','',$string);
        $string = str_replace('&quot;','',$string);
        $string = str_replace('nbsp;','',$string);
        return $string;
    }
    protected function get_date($date){

        return date('Y-m-d H:i:s',strtotime($date));

    }

    protected function clear_author($string){

        $string = strip_tags($string);

        return $string;
    }
    protected function getAllNews(){

        $news=News::model()->model()->get_today_news();
        $all_news=array();
        foreach ($news as $one){
            $all_news[$one->id]=$one;
        }

        return $all_news;
    }
/*    protected function get_news_details($url)
    {
        $html = new SimpleHTMLDOM();

        $html_data = $html->file_get_html($url);

        $data['url']= $url;

        $data['column']= null;

        $data['link_md5'] = md5($url);

        $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        $data['image']['type']='image';

        if(isset($html_data->find('h1[class=articletitle]', 0)->innertext))
            $data['title']= $this->clear_tags($html_data->find('h1[class=articletitle]', 0)->innertext);
        else {
            if(isset($html_data->find('meta[property=og:title]',0)->content))
                $data['title'] =  $this->clear_tags($html_data->find('meta[property=og:title]',0)->content);
            else
                $data['title'] = 'blank title';
        }

        $data['date'] = $this->get_date($this->clear_tags($html_data->find('time[class=date]', 0)->datetime));
        if(isset($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext))
            $data['author'] = $this->clear_author($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext);
        else{
            if(isset($html_data->find('meta[name=author]',0)->content))
                $data['author'] = $this->clear_author($html_data->find('meta[name=author]',0)->content);
            elseif(isset($html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content))
                $data['author'] = $html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content;
            else
                $data['author'] = '';
        }

        $data['number_star']='';
        if(isset($html_data->find('span[id=star-raters-number]', 0)->innertext))
            $data['number_star'] = $this->clear_tags($html_data->find('span[id=star-raters-number]', 0)->innertext);

        $data['description']='';

        if(isset($html_data->find('div[id=articleContent]', 0)->innertext))
            $data['description']= $this->clear_tags($html_data->find('div[id=articleContent]', 0)->innertext);
        elseif(isset($html_data->find('div[id=article-body]', 0)->innertext))
            $data['description']= $this->clear_tags($html_data->find('div[id=article-body]', 0)->innertext);


        $data['description'] = str_replace('لقراءة مقالات سابقة للكاتب يرجى النقر على اسمه .', '',  $data['description']);
        $data['description'] = str_replace('يمكن إرسال سكيك على خدمة «خبرونا» ضمن تطبيقات «الإمارات اليوم» على الهواتف الذكية.', '',  $data['description']);
        $data['description'] = str_replace('نعتذر عن عدم نشر ملاحظات القراء غير المرفق بها اسم ورقم هاتف المرسل.', '',  $data['description']);
        $data['description'] = str_replace('sekeek@emaratalyoum.com', '',  $data['description']);

        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
                $data['image']['src'] = $item->getAttribute('content');
                $data['image']['type']='image';
            }
        }

        if($data['image']['src'] == 'http://cache.emaratalyoum.com/res/img/logo-1024x576.png'){
            $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        }
        if(isset($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext))
            $data['column']= $this->clear_tags($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext);


        $gallary = $html_data->find('div[class=container_12] div[class=grid_8 pull_right content] article div[class=inlinegallery] div[class=articleinlinegallery] ul[class=slides] li img');
        $counter=0;
        foreach ($gallary as $item) {
            $data['gallary']['type']='gallery';
            $data['gallary']['src'][$counter] =  Yii::app()->params['feedUrl'].$item->src;
            $counter++;
        }

        $data['description'] = trim($data['description']);

        return $data;
    }*/

    protected function check_and_update_news($id,$post_id,$platform_id){

        $this->platform_id=$platform_id;
        $this->post_id=$post_id;
        $this->news = News::model()->findByPk($id);


        $this->details=$this->get_news_details($this->news->link);

        if($this->news->title != $this->details['title']){
            $this->category=Category::model()->findByPk($this->news->category_id);
            $this->sub_category=SubCategories::model()->findByPk($this->news->sub_category_id);

            $this->news->title = $this->details['title'];
            $this->news->column = $this->details['column'];
            $this->news->description = isset($this->details['description'])?$this->details['description']:null;
            $this->news->creator = isset($this->details['author'])?$this->details['author']:null;
            //$this->news->schedule_date = $this->model->time;
            if($this->news->save(true)) {
                $this->generator();
            }

        }
    }
    protected function mediaNews($id, $data)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('news_id',$id);
        MediaNews::model()->deleteAll($criteria);


        $media = new MediaNews();
        if($data['type'] == 'image'){

            $media->command=false;
            $media->id=null;
            $media->news_id = $id;
            $media->media = $data['src'];
            $media->type=$data['type'];
            $media->setIsNewRecord(true);
            if(!$media->save()){

            }


        }elseif($data['type'] == 'gallery'){
            foreach ($data['src'] as $item) {
                $empty_media = MediaNews::model()->findByAttributes(array('news_id'=> $id,'media'=>$item));
                if(empty($empty_media)){
                    $media->id=null;
                    $media->news_id = $id;
                    $media->media = $item;
                    $media->type=$data['type'];
                    $media->setIsNewRecord(true);
                    if(!$media->save()){

                    }
                }

            }
        }

    }

    protected function generator(){

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index=>$item) {
            $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
            $this->hash_tag[$index]=' '.trim($item->title).' ';
        }

        $this->parent = null;



        if(isset($this->category->title))
            $this->category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));


        if(isset($this->sub_category->title))
            $this->sub_category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', isset($this->sub_category->title)));

        $this->source = Yii::app()->params['source'].' : ';

        if($this->category == '#'.'أعمدة')
            $this->source = Yii::app()->params['line'].' : ';


        $this->creator = false;

        if($this->news->creator == 'دبي - الإمارات اليوم')
            $this->creator =true;

        $this->platform = Platform::model()->findByPk($this->platform_id);
        $this->post();

        $this->news->generated = 1;

        $this->news->save();
    }

    private function get_template($platform,$category){

        $temp  = PostTemplate::model()->findByAttributes(array(
            'platform_id'=>$platform->id,
            'catgory_id'=>$category,
        ));
        if(!empty($temp))
            return $temp;
        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp  = PostTemplate::model()->find($cond);

        if(!empty($temp))
            return $temp;

        return Yii::app()->params['templates'];
    }

    protected function post(){

        $is_scheduled =1;

        $twitter_is_scheduled =false;

        $media= null;

        $temp = $this->get_template($this->platform,$this->news->category_id);

        $title = $this->news->title;

        $description = $this->shorten_point($this->news->description,'.');

        if($this->platform->title != 'Instagram'){
            $title = str_replace($this->hash_tag, $this->trim_hash, $this->news->title);
            $description = str_replace($this->hash_tag, $this->trim_hash, $this->shorten_point($this->news->description,'.'));
        }

        $title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
        $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

        $description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
        $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

        $shorten_url = $this->news->shorten_url;
        $full_creator = $this->source.$this->news->creator;
        if(!empty(strpos($this->news->creator,':')))
            $full_creator = $this->news->creator;

        $cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


        $text = str_replace(array('[title]','[description]','[short_link]','[author]',),array($title,$description,$shorten_url,$cre),$temp['text']
        );

        if($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter'){
            $text = str_replace('# ', '#', $text);
            $found = true;
            preg_match_all("/#([^\s]+)/", $text, $matches);
            if(isset($matches[0])){
                $matches[0] = array_reverse($matches[0]);
                $count = 0;
                foreach ($matches[0] as $hashtag){
                    if($count>=2)
                    {
                        $found = false;
                        $hashtag_without = str_replace('#','',$hashtag);
                        $hashtag_without = str_replace('_',' ',$hashtag_without);
                        $text = str_replace($hashtag,$hashtag_without,$text);

                    }
                    $count++;
                }

                if($count>=2) {
                    $found = false;
                }
            }

            if($found){
                $text = str_replace(array('[section]', '[sub_section]',), array($this->category, isset($this->sub_category->title)?$this->sub_category->title:null,), $text);
            }else{
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);
            }
        }elseif($this->platform->title=='Instagram'){
            $text = str_replace('# ', '#', $text);
            $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
        }


        if(!empty($this->model->image)){
            $newsMedia = MediaNews::model()->findByAttributes(array('media'=>$this->model->image));
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }
        }else{
            $newsMedia = MediaNews::model()->findAll('(type = "image" or type = "gallery") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $index_media= rand(0,count($newsMedia)-1);
                if(isset($newsMedia[$index_media])){
                    $media = $newsMedia[$index_media]->media;
                }
            }
        }
        if($this->platform->title == 'Twitter'){

            $newsMedia = MediaNews::model()->find('(type = "image") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }

            $text_twitter =$text;

            if($temp['type'] == 'Preview')
                if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    $text =$text.PHP_EOL.$this->news->shorten_url;

            if($this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
                $is_scheduled = 0;
                $twitter_is_scheduled = true;
            }else{
                $twitter_is_scheduled =false;
                $text = $text_twitter.PHP_EOL;
                if($temp['type'] == 'Preview')
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                        $text .=PHP_EOL.$this->news->shorten_url;
            }

        }else{
            if($twitter_is_scheduled){
                if(!$is_scheduled){
                    $is_scheduled = 1;
                }
            }
        }
        $text = str_replace('# #', '#', $text);
        $text = str_replace('##', '#', $text);
        $text = str_replace('&#8220;', '', $text);
        $text = str_replace('&#8221;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        $text = str_replace('#8211;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#160;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&ndash;', '', $text);
        if($this->platform->title != 'Instagram'){
            $text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
            $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
        }

        $this->PostQueue = PostQueue::model()->findByPk($this->post_id);
        //$this->PostQueue->setIsNewRecord(true);
        //$this->PostQueue->id= null;
        $this->PostQueue->type = $temp['type'];
        $this->PostQueue->post = $text;
        //$this->PostQueue->schedule_date = $this->news->schedule_date;
        //$this->PostQueue->catgory_id =  $this->news->category_id;
        //$this->PostQueue->main_category_id =  $this->news->category_id;
        $this->PostQueue->link = $this->news->shorten_url;
        $this->PostQueue->is_posted = 0;
        $this->PostQueue->news_id = $this->news->id;
        $this->PostQueue->post_id =null;
        $this->PostQueue->media_url =$media;
        //$this->PostQueue->settings ='general';
        //$this->PostQueue->is_scheduled =$is_scheduled;
        //$this->PostQueue->platform_id =$this->platform->id;
        //$this->PostQueue->generated ='auto';
        //$this->PostQueue->created_at =date('Y-m-d H:i:s');

        $this->PostQueue->save();



    }

}