<?php
/* @var $this DayController */
/* @var $model DaySettings */
/* @var $create DaySettings */

$this->breadcrumbs=array(
	'Days'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#day-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"></div>
					<div class="col-sm-3" style=" text-align: left;">
						<?php echo TbHtml::link('General Settings',$this->createUrl('/settings'),array('class'=>'btn btn-success btn-sm')) ?>
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<h3 class="page-header">Quick create</h3>
							<?php $this->renderPartial('_form', array('model'=>$create)); ?>
						</div>


						<div class="col-sm-8">
							<h3 class="page-header">Manage</h3>
							<?php
							$this->widget('booster.widgets.TbGridView', array(
								'id'=>'day-grid',
								'type' => 'striped bordered condensed',
								'template' => '{pager}{items}{summary}{pager}',
								'enablePagination' => true,
								'pager' => array(
									'class' => 'booster.widgets.TbPager',
									'nextPageLabel' => 'Next',
									'prevPageLabel' => 'Previous',
									'htmlOptions' => array(
										'class' => 'pull-right'
									)
								),
								'htmlOptions' => array(
									'class' => 'table-responsive'
								),
								'dataProvider' => $model->search(),
								'filter' => $model,
								'columns' => array(
									'id',
									array(
										'class' => 'booster.widgets.TbEditableColumn',
										'name'=>'title',
										'type'=>'raw',

										'visible'=>$model->visible_category_id?true:false,
										'editable' => array(
											'type' => 'select2',
											'url' => $this->createUrl('day/edit'),
											'source' => $model->getDays(),
											'placement' => 'right',
											'inputclass' => 'span1'
										),
									),

									array(
										'class' => 'booster.widgets.TbButtonColumn',
										'header' => 'Options',
										'template' => '{delete}',
										'buttons' => array(
											'delete' => array(
												'label' => 'Delete',
												'icon' => 'fa fa-times',
											),
										)
									),
								)));?>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>