<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
require_once (dirname(__FILE__).'/../../../platforms_api/facebook-php-sdk-v4/src/Facebook/autoload.php');
require_once (dirname(__FILE__).'/../../../platforms_api/codebird-php/src/codebird.php');
//require_once (dirname(__FILE__).'/../../../platforms_api/instagram/Instagram.php');
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	'timeZone' => 'Asia/Dubai',
	// preloading 'log' component
	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.modules.settings.models.*',
		'application.components.*',
		'application.extensions.shorturl.ShortUrl',
		'application.extensions.SimpleHTMLDOM.SimpleHTMLDOM',
		'application.extensions.I18N.Arabic',
		'application.extensions.getid3.getID3',
		'application.extensions.platform.instagram.Instagram',
		'application.vendors.phpwkhtmltopdf.WkHtmlToPdf',
		'application.components.twitter_text.*',
		'application.extensions.phpmailer.JPhpMailer',

        'application.modules.split_report.components.*',
        'application.modules.split_report.models.*',


	),
	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'pdfable'=>array(
			'class'=>'ext.pdfable.pdfable.PdfableModule',
			// Optional: Set path to wkthmltopdf binary
			'bin' => 'application/vendors/phpwkhtmltopdf',
		),
	),

    'commandMap' =>require(dirname(__FILE__).'/commandMap.php'),

	// application components
	'components'=>array(
		's3'=>array(
			'class'=>'ext.s3.ES3',
			'aKey'=>'AKIAIJQZJN4NL3DG767Q',
			'sKey'=>'x6J/+rRFBcvY9C34lnuk0aLV8eusV0baTXHR2Tjf',
		),
		'bitly' => array(
			'class' => 'application.extensions.bitly.VGBitly',
			'login' => 'sortechs', // login name
			'apiKey' => 'R_472a17c976b04c859ade54132de43240', // apikey
			'format' => 'json', // default format of the response this can be either xml, json (some callbacks support txt as well)
		),

		'Shorturl' => array(
			'class' => 'ext.shorturl.ShortUrl',
			'apiKey' => 'AIzaSyCUPPqZuC4lV6tBvsnh4Vn4gmYx4UeFWwA', // apikey
		),

		//'db'=>array(
		//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		//),
		// uncomment the following to use a MySQL database

        'db'=>require(dirname(__FILE__).'/database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
	//'params'=>YII_DEBUG?require(dirname(__FILE__).'/params_staging.php'):require(dirname(__FILE__).'/params.php'),
	'params'=>YII_DEBUG?require(dirname(__FILE__).'/params_staging.php'):require(dirname(__FILE__).'/params.php'),
);