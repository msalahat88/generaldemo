<?php
/* @var $this PostQueueAdminController */
/* @var $model PostQueue */

$this->breadcrumbs=array(
	'Post Queues'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PostQueue', 'url'=>array('index')),
	array('label'=>'Create PostQueue', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#post-queue-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-12 pull-right">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
					</div>
				</div>
				<div class="box-body">

<?php
$this->widget('booster.widgets.TbGridView', array(
		'id'=>'post-queue-grid',
		'type' => 'striped bordered condensed',
		'template' => '{items}{pager}{summary}',
		'enablePagination' => true,
		'pager' => array(
			'class' => 'booster.widgets.TbPager',
			'nextPageLabel' => 'Next',
			'prevPageLabel' => 'Previous',
			'htmlOptions' => array(
				'class' => 'pull-right',
				'filterClass'=>'asdasdasd',
			),

		),
		'htmlOptions' => array(
			'class' => 'table-responsive'
		),
		'filter' => $model,
		'dataProvider' => $model->search_admin(),
		'columns' => array(
			array(
				'name'=>'id',
				'htmlOptions'=>array('width'=>'1%'),
			),
			array(
			'name'=>'post',
				'htmlOptions'=>array('width'=>'10%',
					'style'=>'font-size:12px;text-align: center;'),

			),
			array(
				'name'=>'is_scheduled',
				'value'=>'($data->is_scheduled==1)?"Yes":"No"',
				'htmlOptions'=>array('width'=>'2%' ,
					'style'=>'font-size:12px;text-align: center;'),
				'header'=>'Scheduled'
			),

			array(
				'name'=>'type',
				'htmlOptions'=>array('width'=>'2%',
					'style'=>'font-size:12px;text-align: center;'),

			),	array(
				'name'=>'link',
				'type'=>'raw',
				'htmlOptions'=>array('width'=>'2%',
					'style'=>'font-size:12px;text-align: center;'),
				'value'=>'CHtml::link("Url",$data->link,array("target"=>"_blank"))',
			),

			array(
				'name'=>'media_url',
				'type'=>'raw',
				'value'=>'CHtml::link("Url",$data->media_url,array("target"=>"_blank"))',
				'htmlOptions'=>array('width'=>'3%',
					'style'=>'font-size:12px;text-align: center;' ),
				'header'=>'Media'
			),

			array(
				'name'=>'settings',
				'htmlOptions'=>array('width'=>'2%',
					'style'=>'font-size:12px;text-align: center;' ),
			),array(
				'name'=>'schedule_date',
				'htmlOptions'=>array('width'=>'2%',
					'style'=>'font-size:12px;text-align: center;' ),
			),
			array(
			'name'=>'catgory_id',
				'value'=>'$data->catgory->title',
				'htmlOptions'=>array('width'=>'2%',
					'style'=>'font-size:12px;text-align: center;' ),
			),
			array(
				'name'=>'is_posted',
				'value'=>'($data->is_posted==1)?"Yes":(($data->is_posted==2)?"Not Posted (Please retry)":"No")',
				'htmlOptions'=>array('width'=>'2%' ,
					'style'=>'font-size:12px;text-align: center;'),
			),
			array(
				'name'=>'news_id',
				'type'=>'raw',
				'value'=>'CHtml::link(isset($data->news->title)?$data->news->title:null,Yii::app()->createUrl("/news/".$data->news_id),array("target"=>"_blank"))',
				'htmlOptions'=>array('width'=>'2%',
					'style'=>'font-size:12px;text-align: center;' ),

			),

			array(
				'name'=>'platform_id',
				'value'=>'isset($data->platforms->title)?$data->platforms->title:null',
				'htmlOptions'=>array('width'=>'2%',
				'style'=>'font-size:12px;text-align: center;'
				),
			),
			array(
				'name'=>'generated',
				'htmlOptions'=>array(
					'width'=>'1%',
					'style'=>'font-size:12px;text-align: center;'
					),
			),
			array(
				'name'=>'errors',
				'htmlOptions'=>array('width'=>'2%' ),
			),

			array(
				'name'=>'created_at',
				'htmlOptions'=>array('width'=>'2%' ),
			),array(
				'name'=>'updated_at',
				'htmlOptions'=>array('width'=>'2%' ),
			),array(
				'name'=>'created_by',
				'htmlOptions'=>array('width'=>'2%' ),
			),array(
				'name'=>'updated_by',
				'htmlOptions'=>array('width'=>'2%' ),
			),




			array(
				'class' => 'booster.widgets.TbButtonColumn',
				'header' => 'Options',
				//'template' => '{view}{update}{delete}{activate}{deactivate}',
				//'template' => '{view}{delete}',
				'template' => '{view}',
				'htmlOptions'=>array('width'=>'1%'),
				'buttons' => array(
					'view' => array(
						'label' => 'view',
						'buttonType'=>'url',

						'url'=>'Yii::app()->baseUrl."/postQueue/view/".$data->id',
						'options' => array('target' => '_new'),
					),

					'delete' => array(
						'label' => 'Delete',
						'icon' => 'fa fa-times',
					),
				)
			),
		))

);?>
					<style>
						.checkbox {
							display: block;
							min-height: 0px;
							margin-top: 4px;
							margin-bottom: 0px;
							padding-left: 17px;
						}
						.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
							margin-top: 0;
						}
						.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
							float: left;
							margin-left: -16px;
						}
					</style>



				</div>

			</div>
		</div>
	</div>
</section>