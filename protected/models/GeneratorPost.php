<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class GeneratorPost extends CFormModel
{
    public $link;
    public $time;
    public $submit_to_all = true;
    public $platforms;
    public $anyway;
    public $pinned = 0;
    public $now = false;
    public $image = null;
    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('link,time', 'required'),
            array('link', 'check_emarate'),
            array('submit_to_all,platforms', 'checkBoxes_checked'),

        );
    }
    public function check_emarate($attr){

        $emarate_link   = 'http://www.thenational.ae/';
        $pos = strpos(' '.$this->link, $emarate_link );


        if(!$pos) {
            $this->addError('link', 'only links with (http://www.thenational.ae/) is accepted');
            return false;
        }
        return true;
    }

    public function checkBoxes_checked($attr){
        if(empty($this->submit_to_all) and empty($this->platforms)){
            $this->addError('platforms', 'Please select at least one platform or check submit to all');
            return false;

        }else{
            return true;
        }
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'submit_to_all' => 'submit to all',
            'link' => 'Url ',
            'time'   => 'Scheduled date',
            'now'   => 'Post now ',
        );
    }


}
