<?php
/* @var $this EmailsController */
/* @var $model Emails */

$this->pageTitle = "Emails | Create";


$this->breadcrumbs=array(
	'Emails'=>array('admin'),
	'Create',
);


?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Manage',
										'buttonType' =>'link',
										'url' => array('emails/admin')
									),
								),
								/*'htmlOptions'=>array(
                                    'class'=>'pull-right	'
                                )*/
							)
						);

						?></div>
					<div class="col-sm-3" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
</section>