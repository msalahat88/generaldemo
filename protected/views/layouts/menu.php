
<?PHP if(!Yii::app()->user->isGuest){?>


    <header class="main-header">

        <a href="<?php echo Yii::app()->getBaseUrl(true)?>" class="logo">

            <span class="logo-mini"><b>S</b>RS</span>

            <span class="logo-lg"><?php echo Yii::app()->name?></span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation"  ">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span></a>

<!--        <a href="#" class="btn btn-link" onclick="guiders.show('first');return false"  style="    color: white;font-size:16px; padding-top:12px;margin-top:2px;" ><i class="fa fa-bus"></i>&nbsp;&nbsp;&nbsp;Take a tour</a>
-->
            <div class="navbar-custom-menu" >
                <ul class="nav navbar-nav">
                    <?PHP $model = new PostQueue(); ?>
                    <li class="dropdown messages-menu">
                        <a href="<?PHP echo Yii::app()->params['feedUrl']?>" target="_blank" style="    padding: 0px;margin-top: 12px;    max-width: 30px;">
                            <?php
                           // if(Settings::model()->get_generator_is_running()){
                              //  echo CHtml::image(Yii::app()->baseUrl.'/image/cloud_loading_256.gif','',array('style'=>'    width: 90%;float: right;margin-top: -4px;background-color: rgb(255, 255, 255);padding: 3px;margin-left: 0px;border-radius: 50px;','class'=>'img-responsive'));
                            //}else{ ?>
                                <i class="fa fa-share"></i>
                            <?php //} ?>
                        </a>
                    </li>
                    <li class="dropdown messages-menu">
                        <a href="<?PHP echo Yii::app()->params['facebook']['LinkPage']?>" target="_blank">
                            <i class="fa fa-facebook"></i>
                            <span class="label label-success"><?PHP $model->platform_id = 1; echo $model->NumberPost() ?></span>
                        </a>
                    </li>

                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="<?PHP echo Yii::app()->params['twitter']['LinkPage']?>" target="_blank">
                            <i class="fa fa-twitter"></i>

                            <span class="label label-warning"><?PHP $model->platform_id = 2; echo $model->NumberPost() ?></span>
                        </a>
                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->
                    <li class="dropdown tasks-menu">
                        <a href="<?PHP echo Yii::app()->params['instagram_auth']['LinkPage']?>" target="_blank">
                            <i class="fa fa-instagram"></i>
                            <span class="label label-danger"><?PHP $model->platform_id = 3; echo $model->NumberPost() ?></span>
                        </a>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?PHP echo User::model()->get_image() ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo Yii::app()->user->name?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?PHP echo User::model()->get_image() ?>" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo Yii::app()->user->name?>
                                    <small><?php echo Yii::app()->user->name?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo Yii::app()->CreateUrl('/profileForm/update/',array('id'=>Yii::app()->user->id)) ?>" class="btn btn-success " style="text-indent: -2px;">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo Yii::app()->CreateUrl('site/logout')?>" class="btn btn-danger " style="text-indent: -2px;">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                   <!-- <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>-->
                </ul>
            </div>
        </nav>
    </header>
<?PHP } ?>
<?php

if( Yii::app()->controller->id != 'settings' ) {

    $sections = Yii::app()->createUrl('settings/update/1', array('#' => 'guider=SettingsTour'));

    $this->widget('ext.eguiders.EGuider', array(
            'id' => 'first',
            'title' => 'Tour',
            'buttons' => array(
                array('name' => 'Start', 'classString' => 'tourcolor', 'onclick' => "js:function(){  document.location = '$sections';}"),
                array('name' => 'Exit', 'onclick' => "js:function(){guiders.hideAll();}")
            ), 'description' => '<b>Ready to start the tour press Start</b>',
            'overlay' => true,
            'xButton' => true,
            // look here !! 'show' is true, so that means this guider will be
            // automatically displayed when the page loads
            'autoFocus' => true
        )
    );
}
?>