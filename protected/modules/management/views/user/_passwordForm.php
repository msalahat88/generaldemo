<?php
/* @var $this ProfileFormController */
/* @var $model ProfileForm */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>


    <?php
    echo $form->passwordFieldGroup($model,'new_password',array(

        'labelOptions'=>array(
            'class'=>'col-sm-2 passwords'
        ),
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-8 passwords',

        ),


    ) );
    ?>

            <?php
            echo $form->passwordFieldGroup($model,'confirm_password',array(
                'labelOptions'=>array(
                    'class'=>'col-sm-2 passwords'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-8 passwords',
                ),
            ) );
            ?>


	<div class="form-actions  pull-right" style="margin-bottom: 20px;margin-right:20px;">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model ? 'update' : 'Save'
			)
		); ?>

	</div>
	<?php $this->endWidget(); ?>

</div><!-- form -->