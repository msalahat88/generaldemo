<?php
/* @var $this ProfilePicController */
/* @var $model ProfilePic */

$this->pageTitle = "Profile Pic | Update";

$this->breadcrumbs=array(
	'Profile Pic'=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9">	<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Create', 'url'=>array('create')),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
								),

								/*'htmlOptions'=>array(
									'class'=>'pull-right	'
								)*/
							)
						);

						?></div>
					<div class="col-sm-3" style="text-align: left;">
					<?php echo Yii::app()->params['statement']['previousPage']; ?>



					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
</section>
