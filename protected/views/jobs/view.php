<?php
/* @var $this JobsController */
/* @var $model Jobs */

$this->pageTitle = "Travel | View";

$this->breadcrumbs=array(
	'Travel'=>array('admin'),
	$model->id,
);
?>
<section class="content">
	<?PHP if(Yii::app()->user->hasFlash('create')){ ?>
		<div class="callout callout-success" style="margin-top:20px;">
			<h4>Created successfully. </h4>
		</div>

	<?PHP } if(Yii::app()->user->hasFlash('update')){ ?>
		<div class="callout callout-info" style="margin-top:20px;">
			<h4>Updated successfully. </h4>
		</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
 											array('label' => 'Update', 'url'=>array('update', 'id'=>$model->id)),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
									array('label' => 'Create','buttonType'=>'link', 'url'=>array('create'),								'context' => 'info',
										'context' => 'success',
										'htmlOptions' => array('class' => 'btns-positions'), // for inset effect


									),
								),
								/*	'htmlOptions'=>array(
                                        'class'=>'pull-right	'
                                    )*/
							)
						);
						?></div>
					<div class="col-md-3" style="text-align: left;">
					<?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<div class="col-md-4">
						<h4 style="color:darkblue;">Post : </h4>
					</div>
					<div class="col-md-8">
						<h4><?php echo $model->text; ?></h4>
					</div>
					<div class="col-md-4">
						<h4 style="color:darkblue;">Type</h4>
					</div>
					<div class="col-md-8">
						<h4><?php  echo $model->type; ?></h4>
					</div>
					<div class="col-md-4">
						<h4 style="color:darkblue;">Platform</h4>
					</div>
					<div class="col-md-8">
						<h4> <?php echo $model->platform->title; ?></h4>
					</div>
					<div class="col-sm-4">
						<h4 style="color:darkblue;">Start date</h4>
					</div>
					<div class="col-sm-8">
						<h4><?php echo $model->start_date; ?></h4>
					</div>
					<div class="col-sm-4">
						<h4 style="color:darkblue;">End date</h4>
					</div>
					<div class="col-sm-8">
						<h4><?php echo $model->end_date; ?></h4>
					</div>
					<div class="col-sm-4">
						<h4 style="color:darkblue;">Publish time</h4>
					</div>
					<div class="col-sm-8">
						<h4><?php echo $model->publish_time; ?></h4>
					</div>
					<?php if($model->type != 'Text'){ ?>
						<div class="col-sm-4">
							<h4 style="color:darkblue;"><?php if($model->type == 'Video' or $model->type == 'Image'){ ?>Media Url <?php  }elseif($model->type == 'Preview'){ ?>Link<?php } ?></h4>
						</div>
						<div class="col-sm-8">
							<?php
							if($model->type == 'Image'){
								echo CHtml::image($model->media_url,$model->text,array("class"=>"img-responsive img-tb-grid-view"));
							}elseif($model->type == 'Video'){
								?>
								<video id="my-video" class="video-js embed-responsive-item" controls preload="auto"   data-setup="{}" style="width:100%;">
									<source src="<?php echo $model->media_url ?>" type='video/mp4'>

								</video>
								<?php
							}elseif($model->type == 'Preview'){
								echo '<h4>'. CHtml::link($model->link, $model->link, array('target' => '_blank')) .'</h4>';
							}
							?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
</section>
