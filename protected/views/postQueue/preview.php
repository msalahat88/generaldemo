<?PHP
/* @var $this PostQueueController */
/* @var $data PostQueue */
$date = date_create($data->schedule_date);
$date = date_format($date, "Y-m-d  h:i A");
?>

<?php
if($data->is_posted ==3) {
    ?>
    <style>
        .timeline-body{
            padding-bottom: 0px;

        }
        .timeline-footer{
            margin-top:0px;
            padding:0px;
        }
        .prev-footer{
            margin-bottom:12px;
        }
    </style>
    <?php
}
?>
<script>
    window.App = {};



    $(window).ready(function (){


        $("#Edits_<?php echo $data->id ?>").click(function(){

            $(this).hide();
            $(this).parent().find('#edits_textarea_<?php echo $data->id; ?>').show();
            $(this).parent().find('.sub-button').show();

            App.edit_text = function(id){
                $inputs = $('#edits_textarea_<?php echo $data->id; ?>').val();
                $('#edits_textarea_<?php echo $data->id; ?>').keyup(function() {
                    $inputs = $(this).val();

                });

                console.log($inputs);
                $.post('<?PHP echo CController::createUrl('/postQueue/edit_text/'.$data->id) ?>',{id:id,post:$inputs}, function( data ) {

                    $("#Edits_<?php echo $data->id ?>").html(data);
                    $("#Edits_<?php echo $data->id ?>").show();

                    $('#edits_textarea_<?php echo $data->id; ?>').hide();
                    $('#edit_text<?php echo $data->id ?>').hide();
                });

            };

            $(document).click(function(e) {

                if( e.target.id != 'edits_textarea_<?php echo $data->id; ?>' && e.target.id != "Edits_<?php echo $data->id ?>" && e.target.id != 'edit_text<?php echo $data->id ?>' ) {
                    $("#Edits_<?php echo $data->id ?>").show();
                    $('#edits_textarea_<?php echo $data->id; ?>').hide();
                    $('#edit_text<?php echo $data->id ?>').hide();

                }

            });



        });



    });

</script>

<ul class="timeline timeline2">
    <li>
        <?php
        if ($data->platforms->title == "Facebook")
            echo "<i class=\"fa fa-link bg-blue\"></i>";
        elseif ($data->platforms->title == "Twitter")
            echo "<i class=\"fa fa-link bg-aqua\"></i>";
        else
            echo "<i class=\"fa fa-link bg-purple\"></i>";
        ?>

        <div class="timeline-item" style="<?php if ($data->is_posted == 2) { ?>background-color:#efd8d8;color:#424242; <?php } ?>">
            <span class="time" style="text-align:right"><i class="fa fa-clock-o"></i><b>      <?PHP if(Yii::app()->controller->action->id != 'mainPosted' and Yii::app()->controller->action->id != 'mainPinned') { ?>

                        <?php

                        $this->widget('booster.widgets.TbEditableField',
                            array(

/*                                'text'=>$data->schedule_date,*/


                                'name' => 'schedule_date',
                                'type' => 'combodate',
                                'model' => $data,
                                'attribute' => 'schedule_date',
                                'url' => $this->createUrl('/postQueue/edit'),
                                'placement' => 'bottom',
                                'mode' => 'inline',
                                'format' => 'YYYY-MM-DD HH:mm', //in this format date sent to server
                                'viewformat' => 'MMM DD, YYYY HH:mm', //in this format date is displayed
                                'template' => 'DD / MMM / YYYY HH:mm', //template for dropdowns
                                'combodate' => array('minYear' => date('Y'), 'maxYear' => date('Y', strtotime('+5 years'))),
                               /* 'htmlOptions'=>array(
                                    'class'=>'editable editable-click',
                                ),*/
                            )
                        );

                    }else{
                        echo $data->schedule_date;
                    }            ?></b></span>
            <h3 class="timeline-header"><?php echo CHtml::link($data->catgory->title,array('category/view','id'=>$data->catgory->id)) ?>
                <?php
                if($data->is_scheduled !=0 and $data->is_posted !=1){

                if($data->pinned ==0) {
                    ?>
                    <img id="img_pinned_<?php echo $data->id; ?>" data-toggle="tooltip" title="Pin" style="margin-left:10px;cursor: pointer;"
                         onclick="javascript:App.edit_pined('<?php echo $data->id ?>',0)"
                         src="<?php echo Yii::app()->baseUrl . '/image/pin_black.png'; ?>" width="20" height="20"/>
                    <?php
                }

                else{
                    ?>
                    <img id="img_pinned_<?php echo $data->id; ?>" data-toggle="tooltip" title="Unpin" style="margin-left:10px;cursor: pointer;"  onclick="javascript:App.edit_pined('<?php echo $data->id ?>',1)" src="<?php echo Yii::app()->baseUrl.'/image/note2.png'; ?>" width="20" height="20"/>
                <?php } } ?>
            </h3>
            <div class="timeline-body arabic-direction">
                <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id != 'mainPosted' and Yii::app()->controller->action->id != 'mainPinned') { ?>

                    <p class="timeline_p_preview" id="Edits_<?php echo $data->id ?>" title="click to edit" data-toggle="tooltip"> <?php
                        echo $data->get_hashtags_urls($data->platforms->title,$data->post);
                        ?>
                    </p>
                <?php }else{
                    ?>
                    <p class="timeline_p_preview"> <?php
                        echo $data->get_hashtags_urls($data->platforms->title,$data->post);
                        ?>
                    </p>
                    <?php
                } ?>
      <textarea cols='30' rows='20' name='PostQueue[post]' class='timeline_p_preview form-control' id='edits_textarea_<?php echo $data->id ?>' style="display:none;" >
    <?php
    echo strip_tags($data->post);
    ?>
</textarea>
                <?php echo CHtml::button('Edit text', array('style' => 'display:none;', 'class' => 'btn btn-primary btn-flat btn-xs pull-right sub-button', 'onClick' => 'javascript:App.edit_text('.$data->id.')', 'id' => 'edit_text' . $data->id)); ?>



                <div class="col-sm-12" style="background-color:#fff;padding-top:12px;">
                        <?php echo CHtml::link(CHtml::image($data->media_url,'',array('class'=>'img-thumbnail img-responsive img-class')),$data->link,array('target'=>'_blank'))?>
                    </div>
                <?php if(!empty($data->news)){ ?>
                    <div class="col-sm-12" style="background-color:#fff;padding-top:12px;margin-bottom: 20px;" class="prev-footer">
                        <?php echo CHtml::link('<h6>'.$data->news->title.'</h6>'.'<h5><b>'.$this->shorten($data->news->description,200).'</b></h5>',$data->link,array('target'=>'_blank','style'=>'color:#636262;')); ?>

                    </div>
                    <?php } ?>
                <input type="hidden" value="Preview"/>
                <?php
                if ($data->is_posted == 2) { ?>
                    <div class="errors col-sm-12">
                        <?php
                        echo '<p style="color:red;">' . $data->errors . '</p>';
                        ?>
                    </div>
                <?php } ?>
            </div>
            <?php
            if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'mainPosted') {

                if ($data->platforms->title == 'Facebook') {
                    $get_facebook_engagments = ReportFacebook::model()->findByAttributes(array('row_id' => $data->id));
                    if (isset($get_facebook_engagments)) {
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Likes : <small style="color:black;">' . $get_facebook_engagments->likes . '</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Comments : <small style="color:black;">' . $get_facebook_engagments->comments . '</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Shares : <small style="color:black;">' . $get_facebook_engagments->shares . '</small></h6></div>';
                        echo '<div class="col-xs-12 " style="text-align: center;color:blue"><h6>Total Subscribed : <small style="color:black;">' . $get_facebook_engagments->subscribed . '</small></h6></div>';
                    }else{
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Likes : <small style="color:black;">0</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Comments : <small style="color:black;">0</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Shares : <small style="color:black;">0</small></h6></div>';
                        echo '<div class="col-xs-12 " style="text-align: center;color:blue"><h6>Total Subscribed : <small style="color:black;">0</small></h6></div>';
                    }
                }
                if ($data->platforms->title == 'Twitter') {
                    $get_twitter_engagments = ReportTwitter::model()->findByAttributes(array('row_id' => $data->id));
                    if (isset($get_twitter_engagments)) {
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Retweets : <small style="color:black;">' . $get_twitter_engagments->retweet . '</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Favorites : <small style="color:black;">' . $get_twitter_engagments->favorite . '</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Replays : <small style="color:black;">' . $get_twitter_engagments->reply . '</small></h6></div>';
                    }else{
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Retweets : <small style="color:black;">0</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Favorites : <small style="color:black;">0</small></h6></div>';
                        echo '<div class="col-xs-4" style="color:blue;"><h6>Total Replays : <small style="color:black;">0</small></h6></div>';
                    }
                }
                if ($data->platforms->title == 'Instagram') {
                    $get_instagram_engagments = ReportInstagram::model()->findByAttributes(array('row_id' => $data->id));
                    if (isset($get_instagram_engagments)) {
                        echo '<div class="col-xs-6" style="color:blue;"><h6>Total Comments : <small style="color:black;">' . $get_instagram_engagments->comments . '</small></h6></div>';
                        echo '<div class="col-xs-6" style="color:blue;"><h6>Total Likes : <small style="color:black;">' . $get_instagram_engagments->likes . '</small></h6></div>';
                    }else{
                        echo '<div class="col-xs-6" style="color:blue;"><h6>Total Comments : <small style="color:black;">0</small></h6></div>';
                        echo '<div class="col-xs-6" style="color:blue;"><h6>Total Likes : <small style="color:black;">0</small></h6></div>';
                    }
                }
            }
            ?>
            <div class="timeline-footer" style="margin-top:29px;">
                <?php
                if(Yii::app()->controller->action->id != 'mainPinned'){
                ?>
                <?php
                if($data->is_posted ==3){
                    echo "<div class=''><img src='".Yii::app()->baseUrl.'/image/posting.gif'."' class='img-responsive img-thumbnail'/></div>";
                }else {
                ?>
                <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id != 'mainPosted') { ?>

                    <?php echo CHtml::link('<i class="fa fa-eye" style="font-size: 12px"> Show</i>',Yii::app()->createUrl('/postQueue/view/'.$data->id), array('style'=>'', 'class'=>'btn btn-success btn-flat btn-xs')); ?>


                    <?php echo CHtml::link('<i class="fa fa-pencil-square-o" style="font-size: 12px">  Edit</i>',Yii::app()->createUrl('/postQueue/update/'.$data->id), array('style'=>'', 'class'=>'btn btn-primary btn-flat btn-xs')); ?>


                    <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'main'){ ?>

                        <?php echo  CHtml::button('Push Now', array('class'=>'btn btn-info btn-flat btn-xs','onClick'=>'javascript:App.push_post("#push_post'.$data->id.'")','id'=>'push_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/push_post/".$data->id))) ; ?>

                        <?php echo  CHtml::button('UnSchedule', array('style'=>'','class'=>'btn btn-danger btn-flat btn-xs','onClick'=>'javascript:App.remove_post("#remove_post'.$data->id.'")','id'=>'remove_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/delete/".$data->id))) ; ?>

                    <?php  } ?>

                    <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'mainUn'){ ?>
                        <?php echo CHtml::button('Post', array('style'=>'','class'=>'btn btn-danger btn-flat btn-xs','onClick'=>'javascript:App.activate_post("'.$data->id.'","'.$data->platform_id.'","'.$data->parent_id.'")','id'=>'activate_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/activatePost/".$data->id))); ?>
                    <?PHP } } ?>
                <?PHP if(Yii::app()->controller->id == 'postQueue' and Yii::app()->controller->action->id == 'mainPosted') { ?>

                    <?php echo  CHtml::button('RePost now', array('class'=>'btn btn-info btn-flat btn-xs','onClick'=>'javascript:App.re_post("#re_post'.$data->id.'")','id'=>'re_post'.$data->id,'data-url'=>Yii::app()->createUrl("postQueue/re_post/".$data->id))) ; ?>
                    <?php echo CHtml::link('<i class="fa fa-pencil-square-o" style="font-size: 12px">  Edit and RePost</i>',Yii::app()->createUrl('/postQueue/update_posted/'.$data->id), array('style'=>'', 'class'=>'btn btn-primary btn-flat btn-xs')); ?>
                    <?php
                }}
                ?>
                <?php
                }
                ?>
            </div>
        </div>
        <input type="hidden" value="<?php echo $date ?>" id="time-<?php echo $data->id; ?>"

    </li>
</ul>
