<?php
/* @var $data Category */
/* @var $total DefaultController */
$total_news = count($data->news);
?>

<div class="progress-group">
    <span class="progress-text"><?PHP echo $data->title ?></span>
    <span class="progress-number"><b><?php echo $total_news   ?></b>/<?PHP echo $total ?></span>
    <div class="progress sm">
        <div class="progress-bar progress-bar-<?php echo $color ?>" style="width: <?PHP if($total_news) echo number_format( ($total_news/$total) * 100, 2 )  ?>%"></div>
    </div>
</div>
