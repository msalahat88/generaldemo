<?php

class CoverPhotoController extends BaseController
{


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','edit','repost','form','updateAjax','admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
			/*array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdateAjax($id){

		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);


		if(isset($_POST['CoverPhoto']))
		{

			$media= $model->media_url;
			$model->attributes=$_POST['CoverPhoto'];
			$model->created_at = date('Y-m-d H:i:s');
			$model->is_posted = 0;
			$valid = true;


			$model->media_url = $media;


			if($valid){
				if($model->save()) {

					Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
					$this->redirect(array('admin'));
				}
			}
		}

		$this->renderPartial('update',array(
			'model'=>$model,
		));


	}

	public function actionCreate()
	{
		$model=new CoverPhoto;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['CoverPhoto'])) {
			$model->attributes = $_POST['CoverPhoto'];

			$platforms = $_POST['CoverPhoto'];
			if(empty($model->platform_id)){
				$model->addError('platform_id','Platforms can not be blank');
			}
			if (isset($model->platform_id) and !empty($model->platform_id)) {
				foreach ($platforms['platform_id'] as $items) {

					$model->platform_id = $items;
					$model->created_at = date('Y-m-d H:i:s');
					$valid = true;


					if (!$this->FileEmpty($model, 'media_url')) {
						if (!$this->FileType($model, 'media_url', 'image', 'upload')) {
							$model->addError('media_url', "media cannot be blank");
							$valid = false;
						}

					}
					if (!$this->FileType($model, 'media_url', 'image', 'upload')) {


						$model->addError('media_url', "It seems you Didn't upload a proper image file ..");
						$valid = false;

					} else {
						$model->media_url = $this->uploader($model, 'media_url', 'cover_photo_' . time());
						$valid = true;
					}

					$model->id = null;
					if ($valid) {
						$model->setIsNewRecord(true);
						$model->save();
					}
				}


			}
			if ($model->id != null) {
				Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');

				$this->redirect(array('view', 'id' => $model->id));
			}
			$model->platform_id = $platforms['platform_id'];

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['CoverPhoto']))
		{

			$media= $model->media_url;
			$model->attributes=$_POST['CoverPhoto'];
			$model->created_at = date('Y-m-d H:i:s');
			$model->is_posted = 0;
			$valid = true;

				if (!$this->FileEmpty($model, 'media_url')) {
					if (!$this->GetFileType($model, $media, 'image')) {
						$model->addError('media_url', 'It seems you Didn\'t upload a proper image file ..');
						$valid = false;
					} else {
						$model->media_url = $media;
					}
				} else {
					if(!$this->GetFileType($model,'media_url','image','upload')){
						$model->addError('media_url',"It seems you Didn't upload a proper image file ..");
						$valid=false;
					}else
					$model->media_url = $this->uploader($model, 'media_url', 'post_queue_edit_id' . $model->id . '_' . time());
				}


			if($valid){
				if($model->save()) {
					Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionRepost($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);


		if(isset($_POST['CoverPhoto']))
		{

			$media= $model->media_url;
			$model->attributes=$_POST['CoverPhoto'];
			$model->created_at = date('Y-m-d H:i:s');
			$model->is_posted = 0;
			$valid = true;


					$model->media_url = $media;


			if($valid){
				if($model->save()) {
					Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}

		$this->renderPartial('update',array(
			'model'=>$model,
		));
	}
	public function actionForm($id)
	{
		$model = $this->loadModel($id);

		$this->renderPartial('_form_update',array('model'=>$model,'ajax'=>true));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new CoverPhoto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CoverPhoto']))
			$model->attributes=$_GET['CoverPhoto'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CoverPhoto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CoverPhoto'])) {
			$this->data_search = $_GET['CoverPhoto'];
			$this->data_search($model);
			$model->attributes = $_GET['CoverPhoto'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CoverPhoto the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CoverPhoto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CoverPhoto $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cover-photo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionEdit(){
		if(isset($_POST)){
			if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
				$model = $this->loadModel($_POST['pk']);

				if(!empty($_POST['name'])){
					$name = $_POST['name'];
					$value = $_POST['value'];
					$model->$name = $value;
					if($model->save())
						echo true;
					else
						echo false;
				}
			}

		}
	}
}
