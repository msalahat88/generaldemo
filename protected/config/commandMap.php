<?php
return  array(

    'FacebookFeed' => array(
        'class' => 'application.modules.split_report.commands.FacebookFeedCommand',
    ),

    'TwitterFeed' => array(
        'class' => 'application.modules.split_report.commands.TwitterFeedCommand',
    ),
    'InstagramFeed' => array(
        'class' => 'application.modules.split_report.commands.InstagramFeedCommand',
    ),
);