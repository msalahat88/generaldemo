<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $active
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $gender
 * @property string $background_color
 * @property string $profile_pic
 * @property string $created_at
 * @property string $role
 * @property integer $deleted
 */

class User extends BaseModels
{
 	public $confirm_password;
 	public $new_password;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}



	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('name,username,email', 'required','on'=>'create'),
			array('password,confirm_password', 'required','on'=>'create'),
			array('confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>'Passwords does not match','on'=>'create' ),
			array('email','email'),

			array('username,email','unique','on'=>'create'),
			array('email','check_email'),
			array('name, username, password', 'length', 'max'=>255),
			array('modified_at,role,email', 'safe'),
			// @todo Please remove those attributes that should not be searched.
				array('id, name, username, password, created_at,active,pagination_size,role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
    public function beforeSave() {
        if ($this->isNewRecord){

			$this->password = md5($this->password);
            $this->created_at = new CDbExpression('NOW()');

        }
        return parent::beforeSave();
    }
	public function check_email($att){
		$user=User::model()->findByAttributes(array('email'=>$this->$att));
		if($user && $user->id!=$this->id){
			$this->addError('email', 'Email is already taken');

		}

	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array=array(
			'id' => 'ID',
			'name' => 'Name',
			'username' => 'Username',
			'password' => 'Password',
			'active' => 'Active',
			'background_color' => 'Background skin',
			'gender' => 'Gender',
			'created_at' => 'Created At',
		);
		return array_merge($array,$this->Labels());
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('active',$this->active,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('deleted',0);
		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));
	}

	public function get_number_user(){


		return User::model()->count('deleted=0');

 	}
	public function get_image(){
		$images = User::model()->find('id = '. Yii::app()->user->id);


		if($images->gender == 'm') {
			$image = Yii::app()->BaseUrl . '/temp/dist/img/avatar5.png';
		}else{
			$image = Yii::app()->BaseUrl . '/temp/dist/img/avatar2.png';

		}
		if(isset($images->profile_pic)) {
			if ($images->profile_pic != null) {
				$image = $images->profile_pic;
			}
		}
		return $image;

	}
	public function get_skin(){
		$skin = User::model()->find('id = '. Yii::app()->user->id);

		$skins = 'skin-blue';

		if(isset($skin->background_color)) {
			if ($skin->background_color != null) {
				$skins = $skin->background_color;
			} else {
				$skins = 'skin-blue';
			}
		}
		return $skins;

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
