<?php
/* @var $this HashtagController */
/* @var $model Hashtag */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'hashtag-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',
)); ?>
<!--	<a href="#"  onclick="guiders.show('FirstHashtag');return false"    ><i class="fa fa-question-circle"></i></a>
--><div class="col-sm-3">

	<?php echo $form->textFieldGroup($model,'title'); ?>
</div>
	<div class="col-sm-3">
<!--	<a href="#"  onclick="guiders.show('SecondHashtag');return false"    ><i class="fa fa-question-circle"></i></a>
-->
	<?php echo $form->dropDownListGroup($model,'category_id',array(
		'widgetOptions'=>array(
			'data'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title'),
			'htmlOptions'=>array(
			'empty'=>'Choose One')
		),
	)); ?>
</div>
	<div class="col-sm-2">
	<div class="form-actions  pull-right" style="margin-bottom: 20px">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save'
			)
		); ?>


	</div>
</div>
	<?php $this->endWidget(); ?>

</div><!-- form -->
<!--Tour-->

<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'FirstHashtag',
		'title'        => 'Title',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The title of the hashtag',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Hashtag_title',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'SecondHashtag',
		'title'        => 'Sections',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Sections list ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Hashtag_category_id',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>