<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo CHtml::encode($this->pageTitle);?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <style>
        .main-header .logo {
            font-size: 18px;
        }
        .arabic-direction{
            text-align:left;
            direction: ltr;
        }
        .page-sizes{
            float: none;
            clear: both;
            width:100px;
            margin-top:-15px;
        }

        .pagination{
            min-width: 281px;
            margin-left: 30px;
            padding-bottom:30px;
         }
        ul.dropdown-menu li a {
             /*max-width: 70px;*/
            text-indent: -10px;
        }
        span.required{
            color:red;
        }
        .guider_button{
            font-size:12px;    }
        textarea {
            resize: none;
        }
        .btns-positions{
            margin-left:20px;
        }
        .btns-scheduledposts{
            position: relative;
            left:20px;
        }
    </style>
<script>
    $(document).ajaxStart(function(){
        $('#loading').show();
    }).ajaxStop(function(){
        $('#loading').hide();
    });
</script>

    <?PHP Yii::app()->clientScript->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
    Yii::app()->clientScript->registerCssFile('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/dist/css/AdminLTE.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/dist/css/skins/_all-skins.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/plugins/iCheck/square/blue.css');?>
    <link href="http://vjs.zencdn.net/5.8.0/video-js.css" rel="stylesheet">

    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?PHP if(!Yii::app()->user->isGuest):?>
        <?PHP Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/plugins/morris/morris.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/plugins/jvectormap/jquery-jvectormap-1.2.2.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/plugins/datepicker/datepicker3.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/plugins/daterangepicker/daterangepicker-bs3.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/dist/css/sort.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/dist/css/nusrv.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->BaseUrl.'/temp/datetime_picker/jquery.datetimepicker.css');
        Yii::app()->clientScript->registerScriptFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js');
        Yii::app()->clientScript->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css');

        ?>
    <?PHP endif; ?>

    <!--[if lt IE 9]>
    <?PHP
    Yii::app()->clientScript->registerScriptFile('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js');
    Yii::app()->clientScript->registerScriptFile('https://oss.maxcdn.com/respond/1.4.2/respond.min.js');
    ?>
    <![endif]-->
</head>
<body <?PHP if(!Yii::app()->user->isGuest):

    ?>
    <?PHP if(isset(Yii::app()->session['tab_open']) && Yii::app()->session['tab_open'] ){ ?>
        class="<?PHP echo User::model()->get_skin() ?> sidebar-mini sidebar-collapse fixed"
    <?PHP }else{ ?>
        class="hold-transition <?PHP echo User::model()->get_skin() ?> sidebar-mini fixed"
    <?PHP } ?>
<?php endif; ?>>
<div class="wrapper">
<?php include('menu.php');?>
<?php include('aside.php');?>
<div class="content-wrapper">
    <?PHP if(!Yii::app()->user->isGuest){ ?>
    <section class="content-header">
        <div class="col-sm-12"><h4 style="padding-top: 2px"><?PHP echo  $this->pageTitle ?></h4></div>
        <?php if (isset($this->breadcrumbs)){$this->widget('booster.widgets.TbBreadcrumbs',array('links'=>$this->breadcrumbs,));} ?>
    </section>
    <?PHP } ?>
<?PHP echo $content;?>
</div>
<?php include('footer.php');?>
<?php include('asideTwo.php'); ?>
<!--<div class="control-sidebar-bg"></div>
--></div>

</body>
<?PHP Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/iCheck/icheck.min.js'); ?>
<?PHP if(!Yii::app()->user->isGuest):?>
    <?PHP
    Yii::app()->clientScript->registerScriptFile('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');

    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/sparkline/jquery.sparkline.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/knob/jquery.knob.js');
    Yii::app()->clientScript->registerScriptFile('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/daterangepicker/daterangepicker.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/datepicker/bootstrap-datepicker.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/slimScroll/jquery.slimscroll.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/plugins/fastclick/fastclick.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/dist/js/app.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/datetime_picker/jquery.datetimepicker.full.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/dist/js/instafeed.min.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/dist/js/demo.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->BaseUrl.'/temp/dist/js/sortechs.js');
    $cont = CController::CreateUrl('/site/change_show');
    Yii::app()->clientScript->registerScript('my',"
    $('.sidebar-toggle').click(function(){
    $.get('$cont');
    });

    ");
    ?>
<?PHP endif?>

<script src="http://vjs.zencdn.net/5.8.0/video.js"></script>
</html>
