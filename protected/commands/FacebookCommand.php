<?php
//Yii::import('application.commands.BaseCommand');
class FacebookCommand extends BaseCommand{

    private $id = null;

    private $id_fb = null;

    private $post = null;




    public function run($args)
    {
        $this->TimeZone();
        $data = $this->News();

        if(!empty($data)){
            foreach ($data as $value) {

                //catch the post before posting processing...
                $value->is_posted=3;
                $value->save(false);

               /* if(!empty($value->news_id)){

                    $this->check_and_update_news($value->news_id,$value->id,$value->platform_id);

                    $value=PostQueue::model()->findByPk($value->id);

                }*/


                $this->id = $value->id;
                $valid = false;
                switch ($value->type) {
                    case 'Image':
                        $this->post = $value;
                        if($this->image()){
                            $valid = true;
                        }
                        break;
                    case 'Video':
                        $this->post = $value;
                        if( $this->video()){
                            $valid = true;
                        }
                        break;
                    case 'Text':
                        $this->post = $value;
                        if($this->text()){
                            $valid = true;
                        }
                        break;
                    case 'Preview':
                        $this->post = $value;
                        if($this->Preview()){
                            $valid = true;
                        }
                        break;
                }

                $db = PostQueue::model()->findByPk($this->post->id);
                if($valid){
                    $db->is_posted = 1;
                    $db->post_id = $this->id_fb;
                    $db->command = false;
                    if(!$db->save())
                        $this->send_email('facebook','error on posting to facebook');
                }else{
                    $db->is_posted = 2;
                    $db->post_id = $this->id_fb;
                    $db->command = false;
                    if(!$db->save())
                        $this->send_email('facebook','error on posting to facebook');
                }
                break;
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return PostQueue the News
     * @var $data PostQueue
     * @var $value PostQueue
     * @var $db PostQueue
     */

    private function News(){

       return PostQueue::model()->model()->get_post_facebook();
    }

    /**


     * @var $post PostQueue
     */

    private function image(){

        list($facebook,$PAGE_TOKEN)=$this->Load();
        if(!empty($this->post->media_url)){

            $img = get_headers($this->post->media_url, 1);

            $image = Yii::app()->params['webroot'].'/image/facebook.jpg';

            if(isset($img['Content-Length']) && $img['Content-Length'] < 100){

                file_put_contents($image,file_get_contents(Yii::app()->params['webroot'].'/image/default_social_share.jpg'));

            }else{

                if(@file_get_contents(urldecode($this->post->media_url))){
                    file_put_contents($image,file_get_contents(urldecode($this->post->media_url)));
                }else{
                    $this->error = 'failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found';
                    $valid= false;
                }
            }

            sleep(1);
        }

        $data = [
            'message' =>$this->post->post,
            'source' =>$facebook->fileToUpload($image),
        ];

        try {
            $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/photos', $data, $PAGE_TOKEN);
            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $this->send_email('Facebook','error on posting image to facebook');
            echo 'Graph returned an error: ' . $e->getMessage();
            $this->text();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $this->send_email('Facebook','error on posting image to facebook');

            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            $this->text();
        }

    }






    private function text(){

        list($facebook,$PAGE_TOKEN)=$this->Load();
        $data = [
            'message' =>$this->post->post,
        ];

        try {
            $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/feed', $data, $PAGE_TOKEN);
            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $this->send_email('Facebook','error on posting text to facebook');

            echo 'Graph returned an error: ' . $e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $this->send_email('Facebook','error on posting text to facebook');

            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }

    }

    private function Preview(){



        list($facebook,$PAGE_TOKEN)=$this->Load();

        $link = $this->post->link;

        if(isset($this->post->news))
          if($this->post->link == $this->post->news->shorten_url)
            $link = $this->post->news->link;

        $data = [
            'message' =>$this->post->post,
            'link' => $link,
        ];

        if(!empty($this->post->media_url) && (exif_imagetype($this->post->media_url))){
            $data['source']=$this->post->media_url;
            if(isset($this->post->news)){
                $data['description']=substr($this->post->news->description,0,5000);
                $data['name']=$this->post->news->title;
                //$data['caption']=isset($this->post->news->category)?$this->post->news->category->title:'EMARATALYOUM.COM';
                $data['caption']='The national';
            }
        }

        try {
            $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/feed', $data, $PAGE_TOKEN);
            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $this->send_email('Facebook','error on posting preview to facebook');
            echo 'Graph returned an error: ' . $e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $this->send_email('Facebook','error on posting preview to facebook');
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }

    }

    private function video(){

        list($facebook,$PAGE_TOKEN)=$this->Load();
        $message = $this->post->post;

        if(!empty($this->post->media_url)){
            $ext=explode('.',$this->post->media_url);
            $ext=$ext[count($ext)-1];
            //Store in the filesystem.
            file_put_contents(Yii::app()->params['webroot']."/image/video.".$ext,file_get_contents($this->post->media_url));
            $data = [
                'message' =>$message,
                'description' =>$message,
                'source' =>$facebook->videoToUpload(Yii::app()->params['webroot']."/image/video.".$ext),
            ];

        }else{
            $data = [
                'message' =>$message,
                'description' =>$message,

            ];
        }

        try {

            $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/videos', $data, $PAGE_TOKEN);

            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $this->send_email('Facebook','error on posting video to facebook');

            echo 'Graph returned an error: ' . $e->getMessage();
            $this->text();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $this->send_email('Facebook','error on posting video to facebook');

            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            $this->text();
        }
    }


}